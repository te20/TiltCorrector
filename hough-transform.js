function processImage(file)
{
  if (!file.type.match(/image.*/)) {
    return;
  }

  var srcCanvas = document.getElementById('srcImg');
  var greyCanvas = document.getElementById('greyImg');
  var edgeCanvas = document.getElementById('edgeImg');

  var srcImg = new Image();
  srcImg.onload = function() {
    displayImage(srcCanvas, srcImg);
    toGreyscale(srcCanvas, greyCanvas);
    detectEdge(greyCanvas, edgeCanvas);
  }
	
  var reader = new FileReader();
  reader.onload = function () {
    srcImg.src = reader.result;
  }
  reader.readAsDataURL(file);
}
  
function displayImage(canvas, image)
{
  if (canvas.getContext) {
    var context = canvas.getContext('2d');
    var scale = Math.min(canvas.width / image.width, canvas.height / image.height);
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(image, 0, 0, image.width * scale, image.height * scale);
  }
}

function toGreyscale(srcCanvas, dstCanvas)
{
  var srcContext = srcCanvas.getContext("2d");
  var dstContext = dstCanvas.getContext("2d");
  var width = srcCanvas.width;
  var height = srcCanvas.height;

  var srcImg = srcContext.getImageData(0, 0, width, height);
  var dstImg = dstContext.createImageData(width, height);

  for (var i = 0; i < srcImg.data.length * 4; i += 4) {
    var red = srcImg.data[i];
    var green = srcImg.data[i + 1];
    var blue = srcImg.data[i + 2];
	  
    var grey = parseInt(0.299 * red + 0.587 * green + 0.114 * blue);
	  
    dstImg.data[i] = grey;
    dstImg.data[i + 1] = grey;
    dstImg.data[i + 2] = grey;
    dstImg.data[i + 3] = srcImg.data[i + 3];
  }
	
  dstContext.putImageData(dstImg, 0, 0);
}
  
function detectEdge(srcCanvas, dstCanvas)
{
  var srcContext = srcCanvas.getContext("2d");
  var dstContext = dstCanvas.getContext("2d");
  var width = srcCanvas.width;
  var height = srcCanvas.height;
	
  var srcImg = srcContext.getImageData(0, 0, width, height);
  var dstImg = dstContext.createImageData(width, height);

  var w = width;
  for (var y = 1; y < height - 1; y++) {
    for (var x = 1; x < width - 1; x++) {
      var gx =   (srcImg.data[toIndex(x + 1, y - 1, w)] - srcImg.data[toIndex(x - 1, y - 1, w)])
                 + (srcImg.data[toIndex(x + 1, y,     w)] - srcImg.data[toIndex(x - 1, y,     w)])
                 + (srcImg.data[toIndex(x + 1, y + 1, w)] - srcImg.data[toIndex(x - 1, y + 1, w)]);	
				 
      var gy =   (srcImg.data[toIndex(x - 1, y + 1, w)] - srcImg.data[toIndex(x - 1, y - 1, w)])
                 + (srcImg.data[toIndex(x,     y + 1, w)] - srcImg.data[toIndex(x,     y - 1, w)])
                 + (srcImg.data[toIndex(x + 1, y + 1, w)] - srcImg.data[toIndex(x + 1, y - 1, w)]);		

      var g = Math.sqrt(Math.pow(gx, 2) + Math.pow(gy, 2));

      var i = toIndex(x, y, w);
      dstImg.data[i] = g;
      dstImg.data[i + 1] = g;
      dstImg.data[i + 2] = g;
      dstImg.data[i + 3] = srcImg.data[i + 3];
    }
  }
	
  dstContext.putImageData(dstImg, 0, 0);
}

function toIndex(x, y, width)
{
  return 4 * (x + y * width);
}

